<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\chorizos;

class chorizosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $chorizos = chorizos::all();
        return view('chorizos.index', compact('chorizos'));
       //return $chorizos;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('chorizos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        chorizos::create($request->all());
        return redirect()->route('chorizos.index')->with('success','Producto agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(chorizos $chorizo)
    {
        return view('chorizos.edit',compact('chorizo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chorizo= chorizos::find($id);
        $chorizo->update($request->all());

        return redirect()->route('chorizos.index')->with('success', 'Producto actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chorizo= chorizos::find($id);
        $chorizo->delete();

        return redirect()->route('chorizos.index')->with('success', 'Producto eliminado');
    }
}