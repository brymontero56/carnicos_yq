<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pacientes;
use Illuminate\Support\Facades\Validator;

class pacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       $pacientes = pacientes::select('*')
            ->with('telefono')
            ->get();
       return view('pacientes.index', compact('pacientes'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pacientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $v = Validator::make($request->all(),[
        //     'Nombre'=> 'required | max:10 | unique:ahumado',
        //     'Peso'=> 'required | alpha_num | min:5', 
        //     'Precio'=> 'required | numeric', 
        // ]);

        // if($v->fails())
        // {
        //     return redirect()->back()->withErrors($v->errors())->withInput();
        // }else{
             pacientes::create($request->all());
             return redirect()->route('pacientes.index')->with('success','Paciente agregado con exito');

        // }


        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ahumado $ahumado)
    {
        return view('ahumados.edit',compact('ahumado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pacientes= pacientes::find($id);
        $pacientes->update($request->all());

        return redirect()->route('pacientes.index')->with('success', 'Producto actualizado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ahumado= ahumado::find($id);
        $ahumado->delete();

        return redirect()->route('ahumados.index')->with('success', 'Producto eliminado');        
    }
}
