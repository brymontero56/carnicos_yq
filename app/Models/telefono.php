<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class telefono extends Model
{
    use HasFactory;

    protected $table = 'telefono';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected$fillable = [
        'id',
        'numero',
        'operadora_tel',
        
    ];

}
