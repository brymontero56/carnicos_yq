<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jamon extends Model
{
    use HasFactory;

    protected $table = 'jamon';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected$fillable = [
        'id',
        'Nombre',
        'Peso',
        'Precio',
    ];

}
