<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class chorizos extends Model
{
    use HasFactory;

    protected $table = 'chorizos';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected$fillable = [
        'id',
        'Nombre',
        'Peso',
        'Precio',
    ];

}
