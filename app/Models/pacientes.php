<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class pacientes extends Model
{
    use HasFactory;

    protected $table = 'pacientes';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected$fillable = [
        'id',
        'nombre_paciente',
        'email',
        'ciudad',
        'telefono_id',
    ];

    //Relacion de tablas de uno a uno
    public function telefono(){
        return $this->belongsTo(telefono::class);
    }

}
