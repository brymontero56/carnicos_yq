<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoproduc_has_ahumados', function (Blueprint $table) {
            $table->integer('tipoproduc_id_tipoproduc')->index('fk_tipoproduc_has_ahumados_tipoproduc_idx');
            $table->integer('ahumados_id_ahumados')->index('fk_tipoproduc_has_ahumados_ahumados1_idx');

            $table->primary(['tipoproduc_id_tipoproduc', 'ahumados_id_ahumados']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipoproduc_has_ahumados');
    }
};
