<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoproduc_has_chorizos', function (Blueprint $table) {
            $table->integer('tipoproduc_id_tipoproduc')->index('fk_tipoproduc_has_chorizos_tipoproduc1_idx');
            $table->integer('chorizos_id_chorizos')->index('fk_tipoproduc_has_chorizos_chorizos1_idx');

            $table->primary(['tipoproduc_id_tipoproduc', 'chorizos_id_chorizos']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipoproduc_has_chorizos');
    }
};
