<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chorizos', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('Nombre', 20)->nullable();
            $table->string('Peso', 10)->nullable();
            $table->string('Precio', 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chorizos');
    }
};
