<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoproduc', function (Blueprint $table) {
            $table->integer('id_tipoproduc', true);
            $table->string('chorizos', 10)->nullable();
            $table->string('ahumados', 10)->nullable();
            $table->string('jamon', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipoproduc');
    }
};
