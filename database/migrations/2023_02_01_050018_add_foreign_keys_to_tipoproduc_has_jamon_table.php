<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipoproduc_has_jamon', function (Blueprint $table) {
            $table->foreign(['jamon_id_jamon'], 'fk_tipoproduc_has_jamon_jamon1')->references(['id_jamon'])->on('jamon')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['tipoproduc_id_tipoproduc'], 'fk_tipoproduc_has_jamon_tipoproduc1')->references(['id_tipoproduc'])->on('tipoproduc')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipoproduc_has_jamon', function (Blueprint $table) {
            $table->dropForeign('fk_tipoproduc_has_jamon_jamon1');
            $table->dropForeign('fk_tipoproduc_has_jamon_tipoproduc1');
        });
    }
};
