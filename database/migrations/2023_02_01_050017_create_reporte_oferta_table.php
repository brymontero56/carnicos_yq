<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_oferta', function (Blueprint $table) {
            $table->integer('id_reporte_oferta', true);
            $table->string('tipoproducto', 10)->nullable();
            $table->string('nombre', 10)->nullable();
            $table->string('peso', 10)->nullable();
            $table->string('precio', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reporte_oferta');
    }
};
