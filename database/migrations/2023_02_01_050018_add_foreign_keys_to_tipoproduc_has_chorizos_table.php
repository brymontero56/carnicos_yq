<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipoproduc_has_chorizos', function (Blueprint $table) {
            $table->foreign(['chorizos_id_chorizos'], 'fk_tipoproduc_has_chorizos_chorizos1')->references(['id_chorizos'])->on('chorizos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['tipoproduc_id_tipoproduc'], 'fk_tipoproduc_has_chorizos_tipoproduc1')->references(['id_tipoproduc'])->on('tipoproduc')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipoproduc_has_chorizos', function (Blueprint $table) {
            $table->dropForeign('fk_tipoproduc_has_chorizos_chorizos1');
            $table->dropForeign('fk_tipoproduc_has_chorizos_tipoproduc1');
        });
    }
};
