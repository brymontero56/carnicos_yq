<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoproduc_has_jamon', function (Blueprint $table) {
            $table->integer('tipoproduc_id_tipoproduc')->index('fk_tipoproduc_has_jamon_tipoproduc1_idx');
            $table->integer('jamon_id_jamon')->index('fk_tipoproduc_has_jamon_jamon1_idx');

            $table->primary(['tipoproduc_id_tipoproduc', 'jamon_id_jamon']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipoproduc_has_jamon');
    }
};
