<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipoproduc_has_ahumados', function (Blueprint $table) {
            $table->foreign(['ahumados_id_ahumados'], 'fk_tipoproduc_has_ahumados_ahumados1')->references(['id_ahumados'])->on('ahumados')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['tipoproduc_id_tipoproduc'], 'fk_tipoproduc_has_ahumados_tipoproduc')->references(['id_tipoproduc'])->on('tipoproduc')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipoproduc_has_ahumados', function (Blueprint $table) {
            $table->dropForeign('fk_tipoproduc_has_ahumados_ahumados1');
            $table->dropForeign('fk_tipoproduc_has_ahumados_tipoproduc');
        });
    }
};
