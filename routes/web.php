<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ahumadoController;
use App\Http\Controllers\chorizosController;
use App\Http\Controllers\jamonController;
use App\Http\Controllers\pacientesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/usuarios', function () {
    return view('usuarios');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/mensaje', [App\Http\Controllers\HomeController::class, 'extraer_usuarios']);

Route::resource('ahumados', ahumadoController::class);
Route::resource('chorizos', chorizosController::class);
Route::resource('jamones', jamonController::class);
Route::resource('pacientes', pacientesController::class);
