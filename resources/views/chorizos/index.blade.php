@extends('layouts.app')
@section('content')
    <div class="container">
        <br />
        <h4 align='center'>REPORTE DE PRODUCTOS CHORIZOS</h4>
        <div>
            <a class="btn btn-success" href="{{ route('chorizos.create')}}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{Session::get('success')}}</p>
            </div>            
        @endif
        <div class="input-group-text col-md-12">
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Peso</th>
                <th>Precio</th>
                <th>Acciones</th>
            </tr>
            @foreach ($chorizos as $chorizo)                
            <tr>
                <td>{{$chorizo->id}}</td>
                <td>{{$chorizo->Nombre}}</td>
                <td>{{$chorizo->Peso}}</td>
                <td>{{$chorizo->Precio}}</td>
                <td>
                        <a class="btn btn-primary" href="{{ route('chorizos.edit', $chorizo)}}">
                            <i class="fa-solid fa-pen-to-square"></i>
                        </a>

                        <form action="{{ route('chorizos.destroy',$chorizo->id)}}" method="POST">
                            @csrf
                            @method('DELETE')                        
                            <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    </div>
@endsection