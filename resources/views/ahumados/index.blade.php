@extends('layouts.app')
@section('content')
    <div class="container">
        <br />
        <h4 align='center'>REPORTE DE PRODUCTOS AHUMADOS</h4>
        <div>
            <a class="btn btn-success" href="{{ route('ahumados.create')}}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{Session::get('success')}}</p>
            </div>            
        @endif
        <div class="input-group-text col-md-12">
            <table class="table">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Peso</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                </tr>
                @foreach ($ahumados as $ahumado)                
                <tr>
                    <td>{{$ahumado->id}}</td>
                    <td>{{$ahumado->Nombre}}</td>
                    <td>{{$ahumado->Peso}}</td>
                    <td>{{$ahumado->Precio}}</td>
                    <td>    <a class="btn btn-primary" href="{{ route('ahumados.edit', $ahumado)}}">
                                <i class="fa-solid fa-pen-to-square"></i>
                            </a>
                        
                            <form action="{{ route('ahumados.destroy',$ahumado->id)}}" method="POST">
                                @csrf
                                @method('DELETE')                        
                                <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection