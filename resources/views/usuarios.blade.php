@extends('layouts.app')

@section('content')
    <div class="container">
        <h4 align='center'>REPORTE DE USUARIOS</h4>
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Nombre de Usuario</th>
                <th>Email</th>
            </tr>
            @foreach ($usuarios as $usuario)                
            <tr>
                <td>{{$usuario->id}}</td>
                <td>{{$usuario->name}}</td>
                <td>{{$usuario->username}}</td>
                <td>{{$usuario->email}}</td>
            </tr>
            @endforeach
        </table>
    </div>
@endsection