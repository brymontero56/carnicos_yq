@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Producto</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('jamones.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('jamones.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Nombre del Producto:</strong>
                        <input type="text" name="Nombre" class="form-control"
                            placeholder="Ingrese el nombre del producto">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Peso del Producto:</strong>
                        <input type="text" name="Peso" class="form-control"
                            placeholder="Ingrese el peso del producto">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Precio del Producto:</strong>
                        <input type="text" name="Precio" class="form-control"
                            placeholder="Ingrese el precio del producto">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection