@extends('layouts.app')
@section('content')
    <div class="container">
        <br />
        <h4 align='center'>REPORTE DE PRODUCTOS JAMON</h4>
        <div>
            <a class="btn btn-success" href="{{ route('jamones.create')}}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{Session::get('success')}}</p>
            </div>            
        @endif
        <div class="input-group-text col-md-12">
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Peso</th>
                <th>Precio</th>
                <th>Acciones</th>
            </tr>
            @foreach ($jamones as $jamon)                
            <tr>
                <td>{{$jamon->id}}</td>
                <td>{{$jamon->Nombre}}</td>
                <td>{{$jamon->Peso}}</td>
                <td>{{$jamon->Precio}}</td>
                <td><a class="btn btn-primary" href="{{ route('jamones.edit', $jamon)}}">
                        <i class="fa-solid fa-pen-to-square"></i>
                    </a>

                    <form action="{{ route('jamones.destroy',$jamon->id)}}" method="POST">
                        @csrf
                        @method('DELETE')                        
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
            </table>
        </div>
    </div>
@endsection