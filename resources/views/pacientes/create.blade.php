@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Paciente</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('pacientes.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('pacientes.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Nombre del Paciente:</strong>
                        <input type="text" name="nombre_paciente" class="form-control"
                            placeholder="Ingrese el nombre del producto" value='{{old('nombre_paciente')}}'>                        
                        @error('nombre_paciente')
                            <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input type="text" name="email" class="form-control"
                            placeholder="Ingrese el peso del producto" value='{{old('email')}}'>
                        @error('email')
                            <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Ciudad:</strong>
                        <input type="text" name="ciudad" class="form-control"
                            placeholder="Ingrese el precio del producto" value='{{old('ciudad')}}'>
                        @error('ciudad')
                            <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <strong>Telefono:</strong>
                        <input type="text" name="telefono" class="form-control"
                            placeholder="Ingrese el precio del producto" value='{{old('telefono')}}'>
                        @error('telefono')
                            <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <strong>Operadora:</strong>
                        <input type="text" name="operadora_tel" class="form-control"
                            placeholder="Ingrese el precio del producto" value='{{old('operadora_tel')}}'>
                        @error('operadora_tel')
                            <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection