@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>EDITAR REPORTE ahumado</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('ahumados.index')}}">Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('ahumados.update',$ahumado->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Nombre</strong>
                    <input type="text" name="Nombre" class="form-control" value="{{$ahumado->Nombre}}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Peso</strong>
                    <input type="text" name="Peso" class="form-control" value="{{$ahumado->Peso}}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Precio</strong>
                    <input type="text" name="Precio" class="form-control" value="{{$ahumado->Precio}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection