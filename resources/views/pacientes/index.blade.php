@extends('layouts.app')
@section('content')
    <div class="container">
        <br />
        <h4 align='center'>REPORTE DE PACIENTES</h4>
        <div>
            <a class="btn btn-success" href="{{ route('pacientes.create')}}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{Session::get('success')}}</p>
            </div>            
        @endif
        <div class="input-group-text col-md-12">
            <table class="table">
                <tr>
                    <th>Id</th>
                    <th>Nombre del paciente</th>
                    <th>Email</th>
                    <th>Ciudad</th>
                    <th>Telefono</th>
                    <th>Operadora</th>
                    <th>Acciones</th>
                </tr>
                @foreach ($pacientes as $paciente)                
                <tr>
                    <td>{{$paciente->id}}</td>
                    <td>{{$paciente->nombre_paciente}}</td>
                    <td>{{$paciente->email}}</td>
                    <td>{{$paciente->ciudad}}</td>
                    <td>{{$paciente->telefono->numero}}</td>
                    <td>{{$paciente->telefono->operadora_tel}}</td>
                    
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection